import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/hotel/:id',
    name: 'Hotel',
    component: () => import("../views/Hotel.vue"),
  }
]

const router = new VueRouter({
  routes
})

export default router
