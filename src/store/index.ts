import Vue from 'vue'
import Vuex from 'vuex'
// @ts-ignore
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    hotels: []
  },
  mutations: {
    setHotels(state, hotels) {
      state.hotels = hotels.map(hotel => {
        hotel.description = hotel.body
        if(hotel.description.length > 100) {
          hotel.description = hotel.description.substr(0, 100) + '...'
        }
        hotel.preview = 'https://picsum.photos/id/' + (hotel.id + 10) + '/300/200'
        hotel.photo = 'https://picsum.photos/id/' + (hotel.id + 10) + '/300/400'
        return hotel
      })
    },
  },
  actions: {
    async fetchHotels(ctx, count: number = 30) {
      await axios
          .get('https://jsonplaceholder.typicode.com/posts?_limit=' + count)
          .then(response => {
            if (response.data.length > 0) {
              ctx.commit('setHotels', response.data)
            } else {
              console.log('Error');
            }
          })
          .catch(err => {
            console.log(err.message)
          })
    }
  },
  getters: {
    hotelsList: state => state.hotels,
    getHotelById: state => (id: any) => state.hotels.find(hotel => hotel.id === id),
    hotelsCount: state => state.hotels.length
  },
  modules: {}
})
